using DotNetCoreWebApi.Controllers;
using DotNetCoreWebApi.Services;
using DotNetCoreWebApi.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DotNetCoreTest
{
    [TestClass]
    public class TAutoresControllerTests
    {
        [TestMethod]
        public void Get_SiElAutorNoExiste_SeNosRetornaUn404()
        {
            // preparación
            var idAutor = 1;
            var mock = new Mock<IRepositorioTAutores>();
            mock.Setup(x => x.ObtenerPorId(idAutor)).Returns(default(Autor));
            var autoresController = new TAutoresController(mock.Object);

            // prueba
            var resultado = autoresController.Get(idAutor);

            // Verificación
            Assert.IsInstanceOfType(resultado.Result, typeof(NotFoundResult));
        }

        [TestMethod]
        public void Get_SiElAutorExiste_SeNosRetornaElAutor()
        {
            // Preparación
            var autorMock = new Autor()
            {
                ID = 1,
                Nombre = "Felipe Gavilan"
            };
            var mock = new Mock<IRepositorioTAutores>();
            mock.Setup(x => x.ObtenerPorId(autorMock.ID)).Returns(autorMock);
            var autoresController = new TAutoresController(mock.Object);

            // Prueba
            var resultado = autoresController.Get(autorMock.ID);

            // Verificación
            Assert.IsNotNull(resultado.Value);
            Assert.AreEqual(resultado.Value.ID, autorMock.ID);
            Assert.AreEqual(resultado.Value.Nombre, autorMock.Nombre);
        }
    }
}
