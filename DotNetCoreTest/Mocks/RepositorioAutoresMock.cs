﻿using DotNetCoreWebApi.Entities;
using DotNetCoreWebApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace DotNetCoreTest.Mocks
{
    public class RepositorioAutoresMock : IRepositorioTAutores
    {
        public Autor ObtenerPorId(int id)
        {
            if (id == 0)
            {
                return null;
            }

            return new Autor()
            {
                ID = id,
                Nombre = "Claudia Rodríguez"
            };
        }
    }
}
