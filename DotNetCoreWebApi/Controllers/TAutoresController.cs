﻿using DotNetCoreWebApi.Services;
using DotNetCoreWebApi.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace DotNetCoreWebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TAutoresController : ControllerBase
    {
        private readonly IRepositorioTAutores repositorioTAutores;

        public TAutoresController(IRepositorioTAutores repositorioTAutores)
        {
            this.repositorioTAutores = repositorioTAutores;
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<Autor> Get(int id)
        {
            var autor = repositorioTAutores.ObtenerPorId(id);

            if (autor == null)
            {
                return NotFound();
            }

            return autor;
        }
    }
}
