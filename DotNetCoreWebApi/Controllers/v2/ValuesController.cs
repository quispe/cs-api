﻿using DotNetCoreWebApi.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreWebApi.Controllers.v2
{
    [Route("api/v2/[controller]")]
    [ApiController]
    //[HttpHeaderIsPresent("x-version", "2")]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet(Name = "ObtenerValoresV2")]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "ObtenerValorV2")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost(Name = "CrearValorV2")]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}", Name = "ActualizarValorV2")]
        public void Put(int id, [FromBody] string value)
        {
        }

        /// <summary>
        /// Borrar un elemento especifico
        /// </summary>
        /// <param name="id">Id del elemento a borrar</param>
        // DELETE api/values/5
        [HttpDelete("{id}", Name = "BorrarValorV2")]
        public void Delete(int id)
        {
        }
    }
}
