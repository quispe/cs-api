using DotNetCoreWebApi.Context;
using DotNetCoreWebApi.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DotNetCoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibrosController : ControllerBase
    {
        private readonly DotNetDbContext Context;

        public LibrosController(DotNetDbContext Context)
        {
            this.Context = Context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Libro>> Get()
        {
            return Context.Libros.Include(x => x.Autor).ToList();
        }

        [HttpGet("{id}", Name = "ObtenerLibro")]
        public ActionResult<Libro> Get(int id)
        {
            var libro = Context.Libros.Include(x => x.Autor).FirstOrDefault(x => x.ID == id);
            if (libro == null)
                return NotFound();
            return libro;
        }

        public ActionResult Post([FromBody] Libro libro)
        {
            Context.Libros.Add(libro);
            Context.SaveChanges();
            return new CreatedAtRouteResult("ObtenerLibro", new { id = libro.ID }, libro);
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Libro value)
        {
            if (id != value.ID)
                return BadRequest();
            Context.Entry(value).State = EntityState.Modified;
            Context.SaveChanges();
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult<Libro> Delete(int id)
        {
            var libro = Context.Libros.FirstOrDefault(x => x.ID == id);
            if (libro == null)
                return NotFound();
            Context.Libros.Remove(libro);
            Context.SaveChanges();
            return libro;
        }
    }
}