using AutoMapper;
using DotNetCoreWebApi.Context;
using DotNetCoreWebApi.Entities;
using DotNetCoreWebApi.Helpers;
using DotNetCoreWebApi.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutoresController : ControllerBase
    {
        private readonly DotNetDbContext Context;
        private readonly IMapper mapper;

        public AutoresController(DotNetDbContext Context, IMapper mapper)
        {
            this.Context = Context;
            this.mapper = mapper;
        }

        [HttpGet]
        [HttpGet("Listado")]
        [HttpGet("/Listado")]
        [ServiceFilter(typeof(MiFiltroDeAccion))]
        public async Task<ActionResult<IEnumerable<AutorDTO>>> Get(int numeroDePagina = 1, int cantidadDeRegistros = 10)
        {
            var query = Context.Autores.AsQueryable();

            var totalDeRegistros = query.Count();

            var autores = await query
                .Skip(cantidadDeRegistros * (numeroDePagina - 1))
                .Take(cantidadDeRegistros)
                .ToListAsync();

            Response.Headers["X-Total-Registros"] = totalDeRegistros.ToString();
            Response.Headers["X-Cantidad-Paginas"] =
                ((int)Math.Ceiling((double)totalDeRegistros / cantidadDeRegistros)).ToString();

            var autoresDTO = mapper.Map<List<AutorDTO>>(autores);
            return autoresDTO;
        }

        [HttpGet("Primer")]
        public ActionResult<Autor> GeFirst()
        {
            return Context.Autores.FirstOrDefault();
        }

        [HttpGet("{id}/{optional?}", Name = "ObtenerAutor")]
        public async Task<ActionResult<AutorDTO>> Get(int id)
        {
            var autor = await Context.Autores.Include(x => x.Libros).FirstOrDefaultAsync(x => x.ID == id);
            if (autor == null)
                return NotFound();
            var autorDTO = mapper.Map<AutorDTO>(autor);
            return autorDTO;
        }

        public ActionResult Post([FromBody] AutorCreacionDTO autorCreacion)
        {
            var autor = mapper.Map<Autor>(autorCreacion);
            Context.Autores.Add(autor);
            Context.SaveChanges();
            var autorDTO = mapper.Map<AutorDTO>(autor);
            return new CreatedAtRouteResult("ObtenerAutor", new { id = autor.ID }, autorDTO);
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult> Patch(int id, [FromBody] JsonPatchDocument<AutorCreacionDTO> patchDocument)
        {
            if (patchDocument == null)
                return BadRequest();
            var autorDeLaDB = await Context.Autores.FirstOrDefaultAsync(a => a.ID == id);
            if (autorDeLaDB == null)
                return NotFound();
            var autorDTO = mapper.Map<AutorCreacionDTO>(autorDeLaDB);
            patchDocument.ApplyTo(autorDTO, ModelState);
            var isValid = TryValidateModel(autorDTO);
            if (!isValid)
                return BadRequest(ModelState);
            mapper.Map(autorDTO, autorDeLaDB);
            await Context.SaveChangesAsync();
            return NoContent();
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] AutorCreacionDTO value)
        {
            var autor = mapper.Map<Autor>(value);
            autor.ID = id;
            Context.Entry(autor).State = EntityState.Modified;
            Context.SaveChanges();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult<Autor> Delete(int id)
        {
            var autorID = Context.Autores.Select(a => a.ID).FirstOrDefault(x => x == id);
            if (autorID == default(int))
                return NotFound();
            Context.Autores.Remove(new Autor { ID = autorID });
            Context.SaveChanges();
            return NoContent();
        }
    }
}