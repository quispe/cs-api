﻿using DotNetCoreWebApi.Data;
using DotNetCoreWebApi.Entities;
using DotNetCoreWebApi.Helpers;
using DotNetCoreWebApi.Models;
using DotNetCoreWebApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreWebApi.Controllers.v1
{
    [Route("api/v1/[controller]")]
    //[Route("api/[controller]")]
    [ApiController]
    //[HttpHeaderIsPresent("x-version", "1")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ValuesController : ControllerBase
    {
        private readonly ValuesRepository repository;
        private readonly HashService hashService;
        private readonly IUrlHelper urlHelper;

        public ValuesController(ValuesRepository repository, HashService hashService, IUrlHelper urlHelper)
        {
            this.repository = repository ?? throw new ArgumentException(nameof(repository));
            this.hashService = hashService;
            this.urlHelper = urlHelper;
        }

        // GET api/values
        [HttpGet(Name = "ObtenerValores")]
        [ServiceFilter(typeof(HATEOASValueFilterAttribute))]
        public async Task<ActionResult<IEnumerable<Value>>> Get()
        {
            return await repository.GetAll();
        }

        [HttpGet("hash")]
        public ActionResult GetHash()
        {
            string textoPlano = "Nombre Completo";
            var Result1 = hashService.Hash(textoPlano).Hash;
            var Result2 = hashService.Hash(textoPlano).Hash;
            return Ok(new { textoPlano, Result1, Result2 });
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "ObtenerValor")]
        [EnableCors("PermitirApiRequest")]
        [ServiceFilter(typeof(HATEOASValueFilterAttribute))]
        public async Task<ActionResult<Value>> Get(int id)
        {
            var response = await repository.GetById(id);
            if (response == null) return NotFound();
            return response;
        }

        private void GenerarEnlaces(Value response)
        {
            response.Enlaces.Add(new Enlace(href: urlHelper.Link("ObtenerValor", new { id = response.Id }), rel: "self", metodo: "GET"));
            response.Enlaces.Add(new Enlace(href: urlHelper.Link("ActualizarValor", new { id = response.Id }), rel: "update-valor", metodo: "PUT"));
            response.Enlaces.Add(new Enlace(href: urlHelper.Link("BorrarValor", new { id = response.Id }), rel: "delete-valor", metodo: "DELETE"));
        }

        // POST api/values
        [HttpPost(Name = "CrearValor")]
        public async Task Post([FromBody] Value value)
        {
            await repository.Insert(value);
        }

        // PUT api/values/5
        [HttpPut("{id}", Name = "ActualizarValor")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}", Name = "BorrarValor")]
        public async Task Delete(int id)
        {
            await repository.DeleteById(id);
        }
    }
}
