using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DotNetCoreWebApi.Helpers;

namespace DotNetCoreWebApi.Entities
{
    public class Autor : IValidatableObject
    {
        public int ID { get; set; }
        [Required]
        [PrimeraLetraMayuscula]
        public string Nombre { get; set; }
        public string Identificacion { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public List<Libro> Libros { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!string.IsNullOrEmpty(Nombre))
            {
                var primeraLetra = Nombre[0].ToString();
                if (primeraLetra != primeraLetra.ToUpper())
                {
                    yield return new ValidationResult("La primera letra debe ser mayúscula");
                }

            }
        }
    }
}