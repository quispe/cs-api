﻿using DotNetCoreWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreWebApi.Entities
{
    public class Value : Recurso
    {
        public int Id { get; set; }
        public int Value1 { get; set; }
        public string Value2 { get; set; }
    }
}
