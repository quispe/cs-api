﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreWebApi.Entities
{
    public class HostedServiceLog
    {
        public int ID { get; set; }
        public string Message { get; set; }
    }
}
