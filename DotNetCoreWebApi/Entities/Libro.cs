using System.ComponentModel.DataAnnotations;

namespace DotNetCoreWebApi.Entities
{
    public class Libro
    {
        public int ID { get; set; }
        [Required]
        public string Titulo { get; set; }
        [Required]
        public int AutorID { get; set; }
        public Autor Autor { get; set; }
    }
}