﻿using DotNetCoreWebApi.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreWebApi.Helpers
{
    public class HATEOASValueFilterAttribute : HATEOASFilterAttribute
    {
        private readonly GeneradorEnlaces generadorEnlaces;

        public HATEOASValueFilterAttribute(GeneradorEnlaces generadorEnlaces)
        {
            this.generadorEnlaces = generadorEnlaces ?? throw new ArgumentNullException(nameof(generadorEnlaces));
        }

        public override async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            var incluirHATEOAS = DebeIncluirHATEOAS(context);

            if (!incluirHATEOAS)
            {
                await next();
                return;
            }

            var result = context.Result as ObjectResult;
            var model = result.Value as Value;
            if (model == null)
            {
                var modelList = result.Value as List<Value> ?? throw new ArgumentNullException("Se esperaba una instancia de Value");
                result.Value = generadorEnlaces.GenerarEnlaces(modelList);
                await next();
            }
            else
            {
                generadorEnlaces.GenerarEnlaces(model);
                await next();
            }
        }
    }
}
