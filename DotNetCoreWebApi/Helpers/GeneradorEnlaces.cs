﻿using DotNetCoreWebApi.Entities;
using DotNetCoreWebApi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreWebApi.Helpers
{
    public class GeneradorEnlaces
    {
        private IUrlHelper urlHelper;

        public GeneradorEnlaces(IUrlHelper urlHelper)
        {
            this.urlHelper = urlHelper;
        }

        public ColeccionDeRecursos<Value> GenerarEnlaces(List<Value> value)
        {
            var resultado = new ColeccionDeRecursos<Value>(value);
            value.ForEach(a => GenerarEnlaces(a));
            resultado.Enlaces.Add(new Enlace(urlHelper.Link("ObtenerValores", new { }), rel: "self", metodo: "GET"));
            resultado.Enlaces.Add(new Enlace(urlHelper.Link("CrearValor", new { }), rel: "crear-Value", metodo: "POST"));
            return resultado;
        }

        public void GenerarEnlaces(Value value)
        {
            value.Enlaces.Add(new Enlace(urlHelper.Link("ObtenerValor", new { id = value.Id }), rel: "self", metodo: "GET"));
            value.Enlaces.Add(new Enlace(urlHelper.Link("ActualizarValor", new { id = value.Id }), rel: "actualizar-Value", metodo: "PUT"));
            value.Enlaces.Add(new Enlace(urlHelper.Link("BorrarValor", new { id = value.Id }), rel: "borrar-Value", metodo: "DELETE"));
        }
    }

}