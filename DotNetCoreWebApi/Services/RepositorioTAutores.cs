﻿using DotNetCoreWebApi.Context;
using DotNetCoreWebApi.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreWebApi.Services
{
    public class RepositorioTAutores : IRepositorioTAutores
    {
        private readonly DotNetDbContext context;

        public RepositorioTAutores(DotNetDbContext context)
        {
            this.context = context;
        }

        public Autor ObtenerPorId(int id)
        {
            return context.Autores.FirstOrDefault(x => x.ID == id);
        }
    }
}
