﻿using DotNetCoreWebApi.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreWebApi.Services
{
    public interface IRepositorioTAutores
    {
        Autor ObtenerPorId(int id);
    }
}
