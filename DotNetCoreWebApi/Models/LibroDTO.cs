using System.ComponentModel.DataAnnotations;

namespace DotNetCoreWebApi.Models
{
    public class LibroDTO
    {
        public int ID { get; set; }
        public string Titulo { get; set; }
        public int AutorID { get; set; }
    }
}